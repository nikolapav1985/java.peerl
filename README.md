Assignment 4
------------

- BuggySearchAndSort.java (bug fix on number of search and sort algorithms, check comments for details)

Algorithms implemented
----------------------

- Linear search (go through array and check if an element is there)
- Bubble sort (quadratic time algorithm that keeps running until array is sorted)
- Insert sort (quadratic time algorithm that goes through every element in array and puts every element in correct place)
- Selection sort (quadratic time algorithm that finds proper element for each position in the array)

Bugs fixed
----------

- Linear search (corrected found condition, return true if element found, return false if not found at end of the method)
- Bubble sort (added correct stopping condition, stop once array is sorted)
- Insert sort (added correct check for element in order to put element in correct position)
- Selection sort (added correct check to find required element for each position in the array)

Test environment
----------------

- os lubuntu 16.04 kernel version 4.13.0
- javac version 11.0.5
